package com.example.myapplication_mrhead;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private CheckBox checkBox, checkBox2, checkBox3, checkBox4;
    private ImageView imageView1, imageView2, imageView3, imageView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkBox= findViewById(R.id.checkBox2);
        checkBox2= findViewById(R.id.checkBox3);
        checkBox3= findViewById(R.id.checkBox7);
        checkBox4= findViewById(R.id.checkBox8);
        imageView1 = findViewById(R.id.rambut);
        imageView2 = findViewById(R.id.kumis);
        imageView3 = findViewById(R.id.alis);
        imageView4 = findViewById(R.id.janggut);


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Jika checkbox dicentang (true), sembunyikan gambar.
                // Jika checkbox tidak dicentang (false), tampilkan gambar.
                if (isChecked) {
                    imageView1.setVisibility(View.VISIBLE); // Atau View.GONE jika ingin menghilangkan ruangnya juga.
                } else {
                    imageView1.setVisibility(View.GONE);
                }
            }
        });

        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Jika checkbox dicentang (true), sembunyikan gambar.
                // Jika checkbox tidak dicentang (false), tampilkan gambar.
                if (isChecked) {
                    imageView2.setVisibility(View.VISIBLE); // Atau View.GONE jika ingin menghilangkan ruangnya juga.
                } else {
                    imageView2.setVisibility(View.GONE);
                }
            }
        });

        checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Jika checkbox dicentang (true), sembunyikan gambar.
                // Jika checkbox tidak dicentang (false), tampilkan gambar.
                if (isChecked) {
                    imageView3.setVisibility(View.VISIBLE); // Atau View.GONE jika ingin menghilangkan ruangnya juga.
                } else {
                    imageView3.setVisibility(View.GONE);
                }
            }
        });

        checkBox4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Jika checkbox dicentang (true), sembunyikan gambar.
                // Jika checkbox tidak dicentang (false), tampilkan gambar.
                if (isChecked) {
                    imageView4.setVisibility(View.VISIBLE); // Atau View.GONE jika ingin menghilangkan ruangnya juga.
                } else {
                    imageView4.setVisibility(View.GONE);
                }
            }
        });





    }
}